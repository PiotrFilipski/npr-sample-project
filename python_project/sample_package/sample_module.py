def add_numbers(*args):
    return sum(args)


def multiply_numbers(*args):
    product = 1
    for number in args:
        product *= number
    return product


def concat_strings(*args):
    return "".join(args)
