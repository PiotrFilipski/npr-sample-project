from setuptools import setup

setup(
    name="python_project",
    version="0.0.2",
    packages=["sample_package"],
    url="",
    license="",
    author="Piotr Filipski",
    author_email="",
    description="Sample package for presentation purposes",
)
