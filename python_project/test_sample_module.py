import pytest

from sample_package.sample_module import add_numbers, multiply_numbers, concat_strings


@pytest.mark.parametrize(
    "nums, expected_sum",
    (
        pytest.param((1, 2, 3, 4, 5), 15, id="sum_ints"),
        pytest.param((1.5, 1.2, 8.5), 11.2, id="sum_floats"),
        pytest.param((4.5, 6, 8.2), 18.7, id="sum_mixed"),
    ),
)
def test_add_numbers_success(nums, expected_sum):
    assert add_numbers(*nums) == expected_sum


def test_add_numbers_errors():
    with pytest.raises(TypeError):
        add_numbers("string", 1, 2, 3)


@pytest.mark.parametrize(
    "nums, expected_product",
    (
        pytest.param((1, 2, 3, 4, 5), 120, id="product_ints"),
        pytest.param((1.5, 0.5), 0.75, id="product_floats"),
        pytest.param((4.6, 2), 9.2, id="product_mixed"),
    ),
)
def test_multiply_success(nums, expected_product):
    assert multiply_numbers(*nums) == expected_product


def test_concat_strings():
    assert concat_strings("a", "b", "c") == "abc"
